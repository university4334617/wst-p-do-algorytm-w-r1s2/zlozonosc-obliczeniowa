import random
import time
import matplotlib.pyplot as plt
import math


def sortowanie_babelkowe(L):
    start = time.time()
    n = len(L)
    for i in range(n):
        for j in range(0, n - 1 - i):
            if L[j] > L[j + 1]:
                pom = L[j]
                L[j]=L[j+1]
                L[j+1]=pom
    end = time.time()
    return end - start


def sortowanie_babelkowe_naiwne(L):
    start = time.time()
    n = len(L)
    for i in range(n):
        for j in range(0, n - 1):  
            if L[j] > L[j + 1]:
                pom = L[j]
                L[j]=L[j+1]
                L[j+1]=pom
    end = time.time()
    return end - start


def sortowanie_babelkowe_przerwa(L):
    start = time.time()
    n = len(L)
    A = L.copy()
    for i in range(n):
        for j in range(0, n - 1 - i):
            if L[j] > L[j + 1]:
                pom = L[j]
                L[j]=L[j+1]
                L[j+1]=pom
        if i != 0 and A == L:
            end = time.time()
            return end - start
        A = L.copy()
    end = time.time()
    return end - start

def sortowanie_przez_wybieranie(L):
    start = time.time()
    n = len(L)
    for i in range(n):
        for j in range(i + 1, n):
            if L[i] > L[j]:
                pom = L[i]
                L[i]=L[j]
                L[j]=pom
    end = time.time()
    return end - start

def sortowanie_przez_wstawianie(L):
    start = time.time()
    n = len(L)
    for i in range(n):
        for j in range(0,i):
            if L[i] < L[j]:
                pom = L[i]
                L[i]=L[j]
                L[j]=pom
    end = time.time()
    return end - start


def licznik_czasu(f, L):
    time = 0
    mt = 0 #max time
    st = 0 #sredni time
    for i in range(10):
        time = f(L)
        st += time
        if mt < time:
            mt = time
    st /= 10
    return [mt, st]


def test_algorytmow(funkcja, tab, n):
    time = funkcja(tab)
    if time != 0:
        return n * math.log(n) / time
    else:
        return "too short"



x = -1
while x != "0":
    x = str(input("Podaj które zadanie chcesz rozwiązać (1, 2, 3, 4, 5) (jeśli chcesz zakończyć wciśnij 0): "))
    if x == "1":
        n = int(input("Podaj wieloksc tablicy: "))
        tab = [random.randint(1, 100) for i in range(n)]
        print(licznik_czasu(sortowanie_babelkowe, tab.copy()))
        print(licznik_czasu(sortowanie_przez_wstawianie, tab.copy()))
        print(licznik_czasu(sortowanie_przez_wybieranie, tab.copy()))

    if x == "2":
        dl = [10, 20, 50, 100, 200, 500, 1000]
        lista_sort = [sortowanie_babelkowe, sortowanie_przez_wstawianie, sortowanie_przez_wybieranie]

        sts = {f.__name__: [] for f in lista_sort}
        mts = {f.__name__: [] for f in lista_sort}

        for dlugosc in dl:
            tab = [random.randint(1, 100) for i in range(dlugosc)]
            for funkcja in lista_sort:
                sts[funkcja.__name__].append(licznik_czasu(funkcja, tab.copy())[1])
                mts[funkcja.__name__].append(licznik_czasu(funkcja, tab.copy())[0])

        for funkcja in lista_sort:
            plt.plot(dl, sts[funkcja.__name__], label=f'Srednia {funkcja.__name__}')
            plt.plot(dl, mts[funkcja.__name__], label=f'Max {funkcja.__name__}')

        plt.xlabel("Dlugosc")
        plt.ylabel("Czas [sec]")
        plt.title('Czasy srednie i max')
        plt.legend()
        plt.grid(True)
        plt.show()

    if x == "3":
        print("!!! To zarówno 3 i 4 zadanie !!!")
        tab = [random.randint(1, 100) for i in range(300)]
        print('sortowanie babelkowe (normalne): ', licznik_czasu(sortowanie_babelkowe, tab.copy()))
        print('sortowanie babelkowe z przerwą: ', licznik_czasu(sortowanie_babelkowe_przerwa, tab.copy()))
        print('sortowanie babelkowe naiwne: ', licznik_czasu(sortowanie_babelkowe_naiwne, tab.copy()))

    if x == "4":
        print("W zadaniu 3")

    if x == "5":
        dl = [10, 100, 1000]
        lista_sort = [sortowanie_babelkowe, sortowanie_przez_wstawianie, sortowanie_przez_wybieranie]
        for n in dl:
            tab = [random.randint(1, 100) for i in range(n)]
            for funkcja in lista_sort:
                print(f'{funkcja.__name__} \t {n} \t {test_algorytmow(funkcja, tab.copy(), n)}')

print("Koniec")